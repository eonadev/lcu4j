package de.eonadev.lcu4j.exceptions;

public class EndpointAlreadyInitialized extends Exception {

    public EndpointAlreadyInitialized(String endpoint_name) {
        super("Endpoint " + endpoint_name + " already initialized. Please use the LCU4J-Object to access to it");
    }

}
