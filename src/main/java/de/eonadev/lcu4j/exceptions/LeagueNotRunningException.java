package de.eonadev.lcu4j.exceptions;

public class LeagueNotRunningException extends Exception {
    public LeagueNotRunningException() {
        super("Could not detect running \"League of Legends\"-Client!");
    }
}
