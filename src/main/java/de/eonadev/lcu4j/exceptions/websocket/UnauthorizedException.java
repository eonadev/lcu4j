package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class UnauthorizedException extends APIException {
    //401


    public UnauthorizedException( APIResponse response) {
        super("Unauthorized", response);
    }
}
