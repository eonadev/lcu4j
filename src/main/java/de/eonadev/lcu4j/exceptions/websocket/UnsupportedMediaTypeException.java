package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class UnsupportedMediaTypeException extends APIException {
    //415


    public UnsupportedMediaTypeException(APIResponse response) {
        super("Unsupported Media Type", response);
    }
}
