package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class ForbiddenException extends APIException {
    //403


    public ForbiddenException( APIResponse response) {
        super("Forbidden", response);
    }

}
