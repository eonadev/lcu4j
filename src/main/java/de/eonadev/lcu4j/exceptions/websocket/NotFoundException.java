package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class NotFoundException extends APIException {
    //404


    public NotFoundException(APIResponse response) {
        super("Not found", response);
    }


}
