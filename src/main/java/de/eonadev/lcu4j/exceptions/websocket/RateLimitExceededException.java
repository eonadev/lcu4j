package de.eonadev.lcu4j.exceptions.websocket;

import de.eonadev.lcu4j.exceptions.APIException;
import de.eonadev.lcu4j.utils.APIResponse;

public class RateLimitExceededException extends APIException{
    //429


    public RateLimitExceededException(APIResponse response) {
        super("Rate Limit Exceeded", response);
    }
}
