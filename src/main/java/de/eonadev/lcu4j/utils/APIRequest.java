package de.eonadev.lcu4j.utils;


import de.eonadev.lcu4j.enums.EndpointVersion;
import de.eonadev.lcu4j.enums.LCUEndpoint;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Offer request data
 */
public class APIRequest {

    private String method = "";

    private String http_method = "";

    private LCUEndpoint endpoint = null;

    private ArrayList<String> path_parameters = new ArrayList<>();

    private EndpointVersion endpointVersion = null;

    private String body_parameter = "";

    private APIRequest(){

    }

    public static APIRequest createRequest(String endpoint_method, String http_method, String body_parameter){
        APIRequest a = new APIRequest();

        a.setBody_parameter(body_parameter);
        a.setHttp_method(http_method);

        String url_data[] = endpoint_method.split("/");
        a.setEndpoint(LCUEndpoint.valueOf(url_data[0]));

        a.setMethod(url_data[1]);
        a.setEndpointVersion(EndpointVersion.valueOf(url_data[2]));

        for(int i = 3; i < url_data.length; i++){
            a.addPathParameter(url_data[i]);
        }

        return a;
    }

    public String getHttp_method() {
        return http_method;
    }

    private void setHttp_method(String http_method) {
        this.http_method = http_method;
    }

    public String getMethod() {
        return method;
    }

    private void setMethod(String method) {
        this.method = method;
    }

    public EndpointVersion getEndpointVersion() {
        return endpointVersion;
    }

    private void setEndpointVersion(EndpointVersion endpointVersion) {
        this.endpointVersion = endpointVersion;
    }

    public String getBody_parameter() {
        return body_parameter;
    }

    private void setBody_parameter(String body_parameter) {
        this.body_parameter = body_parameter;
    }

    public ArrayList<String> getPath_parameters() {
        return path_parameters;
    }

    private void addPathParameter(String value){
        path_parameters.add(value);
    }

    public LCUEndpoint getEndpoint() {
        return endpoint;
    }

    private void setEndpoint(LCUEndpoint endpoint) {
        this.endpoint = endpoint;
    }




}
