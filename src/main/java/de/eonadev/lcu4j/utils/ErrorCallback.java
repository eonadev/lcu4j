package de.eonadev.lcu4j.utils;

public interface ErrorCallback {

    void onErrorReceived(Exception e);

}
