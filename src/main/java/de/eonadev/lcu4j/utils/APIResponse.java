package de.eonadev.lcu4j.utils;

/**
 * Generic Response of API calls
 */
public class APIResponse {

    /**
     * Error code like "RPC_ERROR"
     */
    private String error_code = "";

    /**
     * HTTP Code
     */
    private int httpStatus = 0;

    /**
     * Details to implementations
     */
    private Object implementationDetails = null;

    /**
     * message if error occurs
     */
    private String message = "";

    /**
     * object-json if response is not an error
     */
    private String object_string = "";


    private APIRequest request = null;


    APIResponse( APIRequest a) {
        request = a;
    }

    /**
     * Returns the error code
     *
     * @return
     */
    public String getError_code() {
        return error_code;
    }

    /**
     * return the http status code
     *
     * @return
     */
    public int getHttpStatus() {
        return httpStatus;
    }

    /**
     * return the implementationDetails
     *
     * @return
     */
    public Object getImplementationDetails() {
        return implementationDetails;
    }

    /**
     * return the message if an error occurred
     *
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * return the object json if the response is OK
     *
     * @return
     */
    public String getObject_string() {
        return object_string;
    }

    /**
     * set the object json
     *
     * @param json
     */
    public void setObject_string(String json) {
        object_string = json;
    }

    /**
     * set the http status code
     *
     * @param httpStatus
     */
    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    /**
     * Returns the request data
     * @return
     */
    public APIRequest getRequest() {
        return request;
    }
}
