package de.eonadev.lcu4j.utils;

import com.profesorfalken.jpowershell.PowerShell;
import com.profesorfalken.jpowershell.PowerShellResponse;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;

public class Utils {

    /**
     * returns the data for the webclient
     * @param riot_base_dir Directory in which League of Legends is installed
     * @return
     */
    public static Object[] getWebClientData(String riot_base_dir) {
        Object return_data[] = new Object[2];

        //System.out.println(riot_base_dir + "/lockfile");

        File lock_file = new File(riot_base_dir, "lockfile");

        String lock_file_content = "";
        String auth_key_decoded = "riot:";

        try (FileInputStream inp = new FileInputStream(lock_file); InputStreamReader inr = new InputStreamReader(inp); BufferedReader br = new BufferedReader(inr)) {
            lock_file_content = br.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lock_file_content.equalsIgnoreCase("")) {
            String data[] = lock_file_content.split(":");
            return_data[0] = Integer.parseInt(data[2]);
            auth_key_decoded += data[3];
            return_data[1] = new String(Base64.getEncoder().encode(auth_key_decoded.getBytes()));
        }

        //return data format
        //[0] ==> Server-Port  (INT)
        //[1] ==> Authorization-Password (STRING)

        return return_data;
    }

    public static Date convertStringToDate(String time) {
        try {
            return new SimpleDateFormat("yyyy-mm-ddThh:mm:ss.SSZ").parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * returns the base directory in which league of legends is installed!
     *
     * @return if found the base path. if League not running or something else happened returns an empty string
     */
    public static String getRiotBaseDirectory() {
        String base_path = "";
        ArrayList<String> csv_data = new ArrayList<>();
        String line = "";


        //Check if system is windows
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("windows")) {
            PowerShell ps = null;
            try{
                ps = PowerShell.openSession();
                PowerShellResponse resp = ps.executeSingleCommand("Get-Process -Name LeagueClient | Select-Object Id, Name, Path | ConvertTo-Csv -NoTypeInformation");
                String resp_text = resp.getCommandOutput();
                if(!resp_text.contains("NoProcessFoundForGivenName")){
                    // Processes found
                    String lines[] = resp.getCommandOutput().split(System.lineSeparator());
                    for(String s: lines){
                        csv_data.add(s);
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }finally{
                if (ps != null){
                    ps.close();
                }
            }
        } else if (os.contains("mac")) {
            //MAC-Version
            ProcessBuilder ps_builder = new ProcessBuilder();
            ps_builder.command("sh");
            try {
                //start Shell
                Process ps = ps_builder.start();

                //init reader to read csv
                InputStreamReader isr = new InputStreamReader(ps.getInputStream());
                BufferedReader br = new BufferedReader(isr);


                //init writer for command execution
                OutputStreamWriter osr = new OutputStreamWriter(ps.getOutputStream());
                BufferedWriter bw = new BufferedWriter(osr);


                //run command
                bw.write("ps -Ao \"%p,%U,%a\" | grep -i LeagueClient^M");
                bw.flush();

                //Sample output of ps/grep command
                // 2716,user,/opt/riot/RADS/projects/league_client/releases/0.0.0.178/deploy/LeagueClient

                //Read CSV
                int count = 2;
                line = br.readLine();
                while (line != null) {
                    count++;
                    if (count == 3)
                        break;
                    csv_data.add(line);
                    line = br.readLine();
                }

                //close streams
                br.close();
                isr.close();

                bw.close();
                osr.close();

                //close process
                ps.destroy();


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //Not supported
        }

        //interpret CSV-Data
        //check if more than 1 row
        if (csv_data.size() > 1) {
            //at least one process found
            //we only get the first process
            String process = csv_data.get(1);

            //Split data
            String process_data[] = process.split(",");

            //format of array
            //[0] ==> Process-Id (PID)
            //[1] ==> Name of Process / User (MAC)
            //[2] ==> Path of executable

            File process_file = new File(process_data[2].replace("\"", ""));

            File directory = process_file.getParentFile();
            boolean foundRADS = false;

            //Change directory until the directory RADS is found or directory is null
            while (directory != null && !foundRADS) {
                if (directory.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.equalsIgnoreCase("RADS");
                    }
                }).length == 0) {
                    directory = directory.getParentFile();
                } else {
                    foundRADS = true;
                }
            }

            if (foundRADS) {
                base_path = directory.getAbsolutePath();
            }
        }

        return base_path;
    }

}
