package de.eonadev.lcu4j.plugins.lolsummoner;

/**
 * Response for lol-summoner / current-summoner / reroll-points
 */
public class LolSummonerSummonerRerollPoints {

    /**
     * current count of points
     */
    private int currentPoints = 0;

    /**
     * maximum count of possible rerolls
     */
    private int maxRolls = 0;

    /**
     * current count of rerolls
     */
    private int numberOfRolls = 0;

    /**
     * count of points a reroll costs
     */
    private int pointsCostToRoll = 0;

    /**
     * how many points needed to fill a roll possibility
     */
    private int pointsToReroll = 0;


    LolSummonerSummonerRerollPoints() {
    }


    /**
     * returns the current reroll points
     *
     * @return
     */
    public int getCurrentPoints() {
        return currentPoints;
    }

    /**
     * returns the maximum number of Rolls
     *
     * @return
     */
    public int getMaxRolls() {
        return maxRolls;
    }

    /**
     * returns the count rerolls the summoner can use
     *
     * @return
     */
    public int getNumberOfRolls() {
        return numberOfRolls;
    }

    /**
     * return the costs how many points a reroll costs
     *
     * @return
     */
    public int getPointsCostToRoll() {
        return pointsCostToRoll;
    }

    /**
     * returns the amount of points which is needed to get the next reroll
     *
     * @return
     */
    public int getPointsToReroll() {
        return pointsToReroll;
    }
}
