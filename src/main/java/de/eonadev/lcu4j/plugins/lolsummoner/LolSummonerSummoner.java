package de.eonadev.lcu4j.plugins.lolsummoner;

import de.eonadev.lcu4j.enums.Ranks;

/**
 * Response for lol-summoner / current_summoner
 */
public class LolSummonerSummoner {

    /**
     * ID of the current account
     */
    private int accountID = 0;

    /**
     * Display name of the current account (the name everyone sees)
     */
    private String displayName = "";

    /**
     * internal name of the current account
     */
    private String internalName = "";

    /**
     * The name of the highest tear in last season (the highest of all ranked queues)
     */
    private Ranks lastSeasonHighestRank = Ranks.unknown;

    /**
     * How much the account is progressed to the next lever
     */
    private int percentCompleteForNextLevel = 0;

    /**
     * Id of the current used profile icon
     */
    private int profileIconId = 0;

    /**
     * UUID of the player
     */
    private String puuid = "";

    /**
     * Information of ARAM rerollpoints
     */
    private LolSummonerSummonerRerollPoints rerollPoints = null;

    /**
     * ID of the summoner
     */
    private int summonerID = 0;

    /**
     * current level of the summoner
     */
    private int summonerLevel = 0;

    /**
     * amount of experience from the last level
     */
    private int xpSinceLastLevel = 0;

    /**
     * amount of experience needed for next level
     */
    private int xpUntilNextLevel = 0;

    LolSummonerSummoner() {
    }

    /**
     * returns the id of the current account
     *
     * @return
     */
    public int getAccountID() {
        return accountID;
    }

    /**
     * returns the display name of the current account (the name everyone sees)
     *
     * @return
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * returns the internal name of the current account
     *
     * @return
     */
    public String getInternalName() {
        return internalName;
    }

    /**
     * returns the highest rank (of all ranked queues) in last season
     *
     * @return
     */
    public Ranks getLastSeasonHighestRank() {
        return lastSeasonHighestRank;
    }

    /**
     * returns the percentage of experience towards next level
     *
     * @return
     */
    public int getPercentCompleteForNextLevel() {
        return percentCompleteForNextLevel;
    }

    /**
     * returns the id of the current used profile icon
     *
     * @return
     */
    public int getProfileIconId() {
        return profileIconId;
    }

    /**
     * returns the UUID of the player
     *
     * @return
     */
    public String getPuuid() {
        return puuid;
    }

    /**
     * returns the information for aram rerolls
     *
     * @return
     */
    public LolSummonerSummonerRerollPoints getRerollPoints() {
        return rerollPoints;
    }

    /**
     * returns the current summonerID
     *
     * @return
     */
    public int getSummonerID() {
        return summonerID;
    }

    /**
     * returns the level of the summoner
     *
     * @return
     */
    public int getSummonerLevel() {
        return summonerLevel;
    }

    /**
     * returns how much experience the current account backwards to the last point has
     *
     * @return
     */
    public int getXpSinceLastLevel() {
        return xpSinceLastLevel;
    }

    /**
     * returns how much experience is needed for the next level
     *
     * @return
     */
    public int getXpUntilNextLevel() {
        return xpUntilNextLevel;
    }
}
