package de.eonadev.lcu4j.plugins.lolmaps;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;
import de.eonadev.lcu4j.utils.ErrorManager;

public class LolMaps extends BasePlugin {

    /**
     * SINGLETON START (access shall only be over LCU4J possible)
     */

    private static boolean isCreated = false;

    public LolMaps() throws EndpointAlreadyInitialized {
        if (isCreated) {
            throw new EndpointAlreadyInitialized("lol-maps");
        } else {
            isCreated = true;
        }
    }


    /**
     * SINGLETON END
     */


    /**
     * Add a map to the client
     * TODO: Test this method
     *
     * @param x
     */
    public void postMap(LolMapsMap x) {
        APIResponse result = postData("/lol-maps/v1/map", LCU4J.instance.getMain_gson().toJson(x));
        if (result.getHttpStatus() != 200) {
            //TODO: Error handling
            ErrorManager.getInstance().handleAPIError(result);
        }
    }


    /**
     * Return an array with maps. This function will always use V2 method
     *
     * @return
     */
    public LolMapsMap[] getMaps() {
        return getMaps(false);
    }


    /**
     * Return an array with maps
     *
     * @param version1 true if this function shall use the V1 method instead the v2
     * @return
     */
    public LolMapsMap[] getMaps(boolean version1) {
        APIResponse result = null;
        if (version1) {
            result = getData("/lol-maps/v1/maps");
        } else {
            result = getData("/lol-maps/v2/maps");
        }

        if (result.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(result.getObject_string(), LolMapsMap[].class);
        } else {
            ErrorManager.getInstance().handleAPIError(result);
        }
        return null;
    }


    /**
     * Returns a specific map
     *
     * @param id id of the map, which shall return
     * @return
     */
    public LolMapsMap getMap(int id) {
        APIResponse result = getData("/lol-maps/v1/" + String.valueOf(id));
        return getLolMapsMap(result);
    }


    /**
     * Returns a specific map
     *
     * @param id       id of the map, which shall return
     * @param gamemode name of the gamemode
     * @return
     */
    @SuppressWarnings("SpellCheckingInspection")
    public LolMapsMap getMap(int id, String gamemode) {
        APIResponse result = getData("/lol-maps/v2/" + String.valueOf(id) + "/" + gamemode);
        return getLolMapsMap(result);
    }

    /**
     * Returns a specific map
     *
     * @param id          id of the map, which shall return
     * @param gamemode    name of the gamemode
     * @param gamemutator the mutator of the game
     * @return
     */
    public LolMapsMap getMap(int id, String gamemode, String gamemutator) {
        APIResponse result = getData("/lol-maps/v2/" + id + "/" + gamemode + "/" + gamemutator);
        return getLolMapsMap(result);
    }

    /**
     *
     * @param result -
     * @return
     */
    private LolMapsMap getLolMapsMap(APIResponse result) {
        if (result.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(result.getObject_string(), LolMapsMap.class);
        } else {
            ErrorManager.getInstance().handleAPIError(result);
            return null;
        }
    }
}
