package de.eonadev.lcu4j.plugins.lolmaps;

import de.eonadev.lcu4j.enums.GameMode;

/**
 * Contains informations of a map in league
 */
public class LolMapsMap {

    /**
     * Array of assets (images, textes, etc)
     */
    private Object assets = null;

    /**
     * Unknown
     */
    private Object categorizedContentBundles = null;

    /**
     * description of the maps
     */
    private String description = "";

    /**
     * which gamemode
     */
    private GameMode gameMode = GameMode.unknown;

    /**
     * description of the gamemode
     */
    private String gameModeDescription = "";

    /**
     * name of the gamemode
     */
    private String gameModeName = "";

    /**
     * short name of the gamemode
     */
    private String gameModeShortName = "";

    /**
     * mutator of the game
     */
    private String gameMutator = "";

    /**
     * id of the map
     */
    private int id = 0;

    /**
     * is this map a default map
     */
    private boolean isDefault = false;

    /**
     * is this map part of the Random Gamemode rotation
     */
    private boolean isRGM = false;

    /**
     *  id of the map
     */
    private String mapStringId = "";

    /**
     * name of the map
     */
    private String name = "";

    /**
     * platformID of the map
     */
    private String platformID = "";

    /**
     * Name of the platform
     */
    private String platformName = "";

    /**
     * additional properties
     */
    private Object properties = null;

    /**
     * Array of tutorial cards
     */
    private LolMapsTutorialCard tutorialCard[] = null;


    public LolMapsMap(){}

    /**
     * Returns an array of asserts (images, texts, etc)
     * @return
     */
    public Object getAssets() {
        return assets;
    }

    /**
     * get the categorized content bundle (unknown)
     * @return
     */
    public Object getCategorizedContentBundles() {
        return categorizedContentBundles;
    }

    /**
     * returns the description of the map
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * returns the game mode
     * @return
     */
    public GameMode getGameMode() {
        return gameMode;
    }

    /**
     * returns the description of the game mode
     * @return
     */
    public String getGameModeDescription() {
        return gameModeDescription;
    }

    /**
     * returns the name of the game mode
     * @return
     */
    public String getGameModeName() {
        return gameModeName;
    }

    /**
     * returns the short name of the game
     * @return
     */
    public String getGameModeShortName() {
        return gameModeShortName;
    }

    /**
     * returns the mutator for the game
     * @return
     */
    public String getGameMutator() {
        return gameMutator;
    }

    /**
     * returns the id of the map
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * returns if it is a default map
     * @return
     */
    public boolean isDefault() {
        return isDefault;
    }

    /**
     * returns if the map is part of the random gamemode rotation
     * @return
     */
    public boolean isRGM() {
        return isRGM;
    }

    /**
     * returns the map string id
     * @return
     */
    public String getMapStringId() {
        return mapStringId;
    }

    /**
     * returns the name of the map
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * returns the platform id
     * @return
     */
    public String getPlatformID() {
        return platformID;
    }

    /**
     * returns the platform name
     * @return
     */
    public String getPlatformName() {
        return platformName;
    }

    /**
     * returns additional properties
     * @return
     */
    public Object getProperties() {
        return properties;
    }

    /**
     * returns an array of tutorial cards
     * @return
     */
    public LolMapsTutorialCard[] getTutorialCard() {
        return tutorialCard;
    }


    public void setAssets(Object assets) {
        this.assets = assets;
    }

    public void setCategorizedContentBundles(Object categorizedContentBundles) {
        this.categorizedContentBundles = categorizedContentBundles;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
    }

    public void setGameModeDescription(String gameModeDescription) {
        this.gameModeDescription = gameModeDescription;
    }

    public void setGameModeName(String gameModeName) {
        this.gameModeName = gameModeName;
    }

    public void setGameModeShortName(String gameModeShortName) {
        this.gameModeShortName = gameModeShortName;
    }

    public void setGameMutator(String gameMutator) {
        this.gameMutator = gameMutator;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public void setRGM(boolean RGM) {
        isRGM = RGM;
    }

    public void setMapStringId(String mapStringId) {
        this.mapStringId = mapStringId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public void setProperties(Object properties) {
        this.properties = properties;
    }

    public void setTutorialCard(LolMapsTutorialCard[] tutorialCard) {
        this.tutorialCard = tutorialCard;
    }
}
