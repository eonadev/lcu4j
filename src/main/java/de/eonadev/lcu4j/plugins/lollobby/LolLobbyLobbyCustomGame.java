package de.eonadev.lcu4j.plugins.lollobby;

public class LolLobbyLobbyCustomGame {

    private int filledPlayerSlots = 0;
    private int filledSpectatorSlots = 0;
    private String gameType = "";
    private boolean hasPassword = false;
    private long id = 0;
    private String lobbyName = "";
    private int mapID = 0;
    private int maxPlayerSlots = 0;
    private int maxSpectatorSlots = 0;
    private String ownerSummonerName = "";
    private String passbackURL = "";
    private String spectatorPolicy = "";


    LolLobbyLobbyCustomGame(){}


    public int getFilledPlayerSlots() {
        return filledPlayerSlots;
    }

    public int getFilledSpectatorSlots() {
        return filledSpectatorSlots;
    }

    public String getGameType() {
        return gameType;
    }

    public long getId() {
        return id;
    }

    public String getLobbyName() {
        return lobbyName;
    }

    public int getMapID() {
        return mapID;
    }

    public int getMaxPlayerSlots() {
        return maxPlayerSlots;
    }

    public int getMaxSpectatorSlots() {
        return maxSpectatorSlots;
    }

    public String getOwnerSummonerName() {
        return ownerSummonerName;
    }

    public String getPassbackURL() {
        return passbackURL;
    }

    public String getSpectatorPolicy() {
        return spectatorPolicy;
    }


    public boolean hasPassword(){
        return this.hasPassword;
    }

}
