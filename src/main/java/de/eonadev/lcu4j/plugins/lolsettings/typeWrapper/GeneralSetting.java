package de.eonadev.lcu4j.plugins.lolsettings.typeWrapper;

import de.eonadev.lcu4j.exceptions.SettingNotFoundException;
import de.eonadev.lcu4j.plugins.lolsettings.BaseSetting;
import de.eonadev.lcu4j.plugins.lolsettings.BaseTypeWrapper;

/**
 * Wrapper for general settings
 * namespace = account
 * ppType = LCUPreferences
 * category = lol-general
 */
public class GeneralSetting extends BaseTypeWrapper {

    public GeneralSetting(BaseSetting obj){
        super(obj);
    }

    /**
     * returns if newbie tips is enabled
     * @return true if newbie tips are shown; false if not
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public boolean isNewbieTipsEnabled() throws SettingNotFoundException {
        return (boolean) getSource_object().getData("newbieTipsEnabled");
    }

    /**
     * enabling newbie tips
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void enableNewbieTips() throws SettingNotFoundException{
        getSource_object().setData("newbieTipsEnabled",true);
    }

    /**
     * disabling newbie tips
     * @throws SettingNotFoundException if the setting key is not found, the source BaseSetting object may not fit for this wrapper
     */
    public void disableNewbieTips() throws SettingNotFoundException{
        getSource_object().setData("newbieTipsEnabled",false);
    }

}
