package de.eonadev.lcu4j.plugins.lolsettings;

import de.eonadev.lcu4j.exceptions.SettingNotFoundException;

import java.util.HashMap;

public class BaseSetting {

    private HashMap<String,Object> data = new HashMap<>();
    private int schemaVersion = 0;


    public Object getData(String key) throws SettingNotFoundException{
        if(data.containsKey(key)){
            return data.get(key);
        }else{
            throw new SettingNotFoundException(key);
        }
    }


    public void setData(String key,Object obj) throws SettingNotFoundException{
        if(data.containsKey(key)){
            data.replace(key,obj);
        }else{
            throw new SettingNotFoundException(key);
        }
    }

    public int getSchemaVersion(){
        return schemaVersion;
    }


}


