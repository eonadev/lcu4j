package de.eonadev.lcu4j.plugins.lolactiveboosts;

import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.exceptions.EndpointAlreadyInitialized;
import de.eonadev.lcu4j.plugins.BasePlugin;
import de.eonadev.lcu4j.utils.APIResponse;
import de.eonadev.lcu4j.utils.ErrorManager;

/**
 * Endpoint for plugin lol-active-boosts
 */
public class LolActiveBoosts extends BasePlugin {


    /**
     * SINGLETON START (access shall only be over LCU4J possible)
     */

    private static boolean is_created = false;

    public LolActiveBoosts() throws EndpointAlreadyInitialized {
        if (is_created) {
            throw new EndpointAlreadyInitialized("lol-active-boosts");
        } else {
            is_created = true;
        }
    }


    /**
     * SINGLETON END
     */

    /**
     * return the current active boosts for the logged in user
     *
     * @return A LolActiveBoostsResponse object on success, otherwise null
     */
    public LolActiveBoostsResponse getActiveBoosts() {
        APIResponse data = getData("/lol-active-boosts/v1/active-boosts");

        if (data.getHttpStatus() == 200) {
            return LCU4J.instance.getMain_gson().fromJson(data.getObject_string(), LolActiveBoostsResponse.class);
        } else {
            ErrorManager.getInstance().handleAPIError(data);
            return null;
        }
    }


}
