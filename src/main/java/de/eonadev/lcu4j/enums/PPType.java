package de.eonadev.lcu4j.enums;

public enum PPType  {

    LCUPreferences("LCUPreferences");


    private String x_lcupreference = "";

    PPType(String s){
        x_lcupreference = s;
    }

    public String getValue() {
        return x_lcupreference;
    }
}
