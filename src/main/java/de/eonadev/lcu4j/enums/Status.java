package de.eonadev.lcu4j.enums;

public enum Status {
    unknown("unknown"),
    online("online"),
    offline("offline"),
    degraded("degraded"),
    deploying("deploying");

    private String x_status = "";

    Status(String s) {
        x_status = s;
    }


    public String getValue(){
        return x_status;
    }
}
