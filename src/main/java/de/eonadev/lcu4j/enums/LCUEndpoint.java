package de.eonadev.lcu4j.enums;

public enum LCUEndpoint {
    lol_account_verification("lol-account-verification"),
    lol_acs("lol-acs"),
    lol_active_boosts("lol-active-boosts"),
    lol_banners("lol-banners"),
    lol_career_stats("lol-career-stats"),
    lol_champ_select("lol-champ-select"),
    lol_champ_select_legacy("lol-champ-select-legacy"),
    lol_champions("lol-champions"),
    lol_chat("lol-chat"),
    lol_clash("lol-clash"),
    lol_clubs("lol-clubs"),
    lol_clubs_public("lol-clubs-public"),
    lol_collections("lol-collections"),
    lol_content_targeting("lol-content-targeting"),
    lol_email_verification("lol-email-verification"),
    lol_end_of_game("lol-end-of-game"),
    lol_esport_stream_notifications("lol-esport-stream-notifications"),
    lol_featured_modes("lol-featured-modes"),
    lol_game_client_chat("lol-game-client-chat"),
    lol_game_queues("lol-game-queues"),
    lol_game_session("lol-game-session"),
    lol_gameflow("lol-gameflow"),
    lol_geoinfo("lol-geoinfo"),
    lol_highlights("lol-highlights"),
    lol_honor_v2("lol-honor-v2"),
    lol_inventory("lol-inventory"),
    lol_item_sets("lol-items-set"),
    lol_kickout("lol-kickout"),
    lol_kr_playtime_reminder("lol-kr-playtime-reminder"),
    lol_kr_shutdown_law("lol-kr-shutdown-law"),
    lol_leaver_buster("lol-leaver-buster"),
    lol_license_agreement("lol-license-agreement"),
    lol_loadouts("lol-loadouts"),
    lol_lobby("lol-lobby"),
    lol_loot("lol-loot"),
    lol_loyalty("lol-loyalty"),
    lol_maps("lol-maps"),
    lol_match_history("lol-match-history"),
    lol_matchmaking("lol-matchmaking"),
    lol_missions("lol-missions"),
    lol_npe_rewards("lol-npe-rewards"),
    lol_npe_tutorial_path("lol-npe-tutorial-path"),
    lol_patch("lol-patch"),
    lol_perks("lol-perks"),
    lol_personalized_offers("lol-presonalized-offers"),
    lol_pft("lol-pft"),
    lol_platform_config("lol-platform-config"),
    lol_player_behavior("lol-player-behavior"),
    lol_player_level_up("lol-player-level-up"),
    lol_player_messaging("lol-player-messaging"),
    lol_player_preferences("lol-player-preferences"),
    lol_pre_end_of_game("lol-pre-end-of-game"),
    lol_premade_voice("lol-premade-voice"),
    lol_purchase_widget("lol-purchase-widget"),
    lol_queue_eligibility("lol-queue-eligibility"),
    lol_ranked("lol-ranked"),
    lol_ranked_stats("lol-ranked-stats"),
    lol_recommendations("lol-recommendations"),
    lol_regalia("lol-regalia"),
    lol_replays("lol-replays"),
    lol_service_status("lol-service-status"),
    lol_settings("lol-settings"),
    lol_shutdown("lol-shutdown"),
    lol_simple_dialog_messages("lol-simple-dialog-messages"),
    lol_spectator("lol-spectator"),
    lol_store("lol-store"),
    lol_suggested_players("lol-suggested-players"),
    lol_summoner("lol-summoner"),
    lol_tencent_qt("lol-tencent-qt"),
    lol_trophies("lol-trophies"),
    lol_worlds_token_card("lol-worlds-token-card");


    private String x_lcuendpoint = "";

    LCUEndpoint(String s) {
        x_lcuendpoint = s;
    }

    public String getLCUendpoint() {
        return x_lcuendpoint;
    }


}
