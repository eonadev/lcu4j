package de.eonadev.lcu4j.enums;

public enum Severity {

    info("info"),
    warn("warn"),
    error("error");


    private String x_severity = "";

    Severity(String s) {
        x_severity = s;
    }

    public String getValue(){
        return x_severity;
    }
}
