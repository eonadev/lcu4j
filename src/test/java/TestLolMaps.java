import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.enums.GameMode;
import de.eonadev.lcu4j.enums.LCUEndpoint;
import de.eonadev.lcu4j.plugins.lolmaps.LolMaps;
import de.eonadev.lcu4j.plugins.lolmaps.LolMapsMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestLolMaps {

    private static LCU4J api = null;
    private static LolMaps lm = null;

    @Test
    @BeforeAll
    public static void initAPI(){
        api = new LCU4J();
        lm = (LolMaps) api.getEndpoint(LCUEndpoint.lol_maps);
    }


    @Test
    @DisplayName("Get all maps using version 1")
    public void testGetMapsVersion1(){
        LolMapsMap maps[] = lm.getMaps(true);

        Assertions.assertNotEquals(0,maps.length);

        for(LolMapsMap m:maps){
            System.out.println(m.getName());
        }
    }

    @Test
    @DisplayName("Get all maps using version 2")
    public void testGetMapsVersion2(){
        LolMapsMap maps[] = lm.getMaps();

        Assertions.assertNotEquals(0,maps.length);

        for(LolMapsMap m:maps){
            System.out.println(m.getName());
        }
    }


    @Test
    @DisplayName("Get all map using a specific ID (Twisted Treeline ==> 10)")
    public void testGetMapTwistedTreeline(){
        LolMapsMap map = lm.getMap(10);

        Assertions.assertNotNull(map,"Couldn`t retrieve Twisted Treeline");
        System.out.println(map.getName());
    }

    @Test
    @DisplayName("Get all map using a specific ID and game mode(Twisted Treeline ==> 10, gamemode ==> CLASSIC)")
    public void testGetMapTwistedTreelineCLASSIC(){
        LolMapsMap map = lm.getMap(10,"CLASSIC");

        Assertions.assertNotNull(map,"Couldn`t retrieve Twisted Treeline CLASSIC");
        System.out.println(map.getName());
    }

    @Test
    @DisplayName("Get all map using a specific ID, game mode and game mutator (Twisted Treeline ==> 10, game mode ==> CLASSIC, game mutator ==> '')")
    public void testGetMapTwistedTreelineCLASSICGameMutator(){
        LolMapsMap map = lm.getMap(10,"CLASSIC","");

        Assertions.assertNotNull(map,"Couldn`t retrieve Twisted Treeline CLASSIC with game mutator ''");
        System.out.println(map.getName());
    }

    @Test
    @DisplayName("Try to put a custom map into league client")
    public void testPostMap(){
        LolMapsMap myMap = new LolMapsMap();
        myMap.setDefault(false);
        myMap.setDescription("That is a custom map ");
        myMap.setGameMode(GameMode.classic);
        myMap.setGameModeDescription("Just kill the others :D");
        myMap.setGameModeName("Custom Map");
        myMap.setGameModeShortName("CM");
        myMap.setName("Custom Map Palatinate Gaming");
        myMap.setId(10);
        myMap.setMapStringId("TT");
        myMap.setRGM(false);

        Assertions.assertDoesNotThrow(() -> {lm.postMap(myMap);},"");


    }




}
