import de.eonadev.lcu4j.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestUtils {

    @Test
    @DisplayName("Testing if the utils can get the base directory of League of Legends")
    public void testGetRiotBaseDirectory(){
        String x = "";
        x = Utils.getRiotBaseDirectory();
        Assertions.assertNotEquals("",x,"Riot Base directory not found!");
    }


    @Test
    @DisplayName("Testing if the utils can get the webserver data")
    public void testGetWebClientData(){
        String base_dir = Utils.getRiotBaseDirectory();
        Object data[] = Utils.getWebClientData(base_dir);
        Assertions.assertNotEquals(null, data,"Webserver data not found");
    }




}
