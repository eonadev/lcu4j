import de.eonadev.lcu4j.LCU4J;
import de.eonadev.lcu4j.plugins.lolsummoner.LolSummonerSummoner;
import de.eonadev.lcu4j.utils.APIResponse;
import de.eonadev.lcu4j.utils.ApacheWebClient;

public class TestApache {


    public static void main(String args[]){
        LCU4J api = new LCU4J();

        ApacheWebClient x = api.getClient();

        try{
            x.openClient();

            APIResponse t = x.executeGET("lol-summoner/v1/current-summoner");
            LolSummonerSummoner s = LCU4J.instance.getMain_gson().fromJson(t.getObject_string(),LolSummonerSummoner.class);

            System.out.println(s.getDisplayName());

            x.closeClient();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
